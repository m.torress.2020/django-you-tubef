import sys
import urllib.request

from django.apps import AppConfig
from .ytparser import YTChannel




class DjangoyoutubeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangoyoutube'

    def ready(self):
        from .models import Videos
        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=UC300utwSVAYOoRLEqmsprfg'  # + sys.argv[1]
        xmlStream = urllib.request.urlopen(url)
        canal = YTChannel(xmlStream)
        videos = canal.videos()
        obj = Videos.objects.all()
        obj.delete()
        for video in videos:
            v = Videos(title=video['title'],
                            url=video['url'],
                            id_video=video['id_video'],
                            selected=False)
            v.save()
