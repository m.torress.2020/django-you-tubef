from django.db import models

class Videos(models.Model):
    title = models.TextField()
    url = models.URLField()
    id_video = models.CharField(max_length=128)
    selected = models.BooleanField()
