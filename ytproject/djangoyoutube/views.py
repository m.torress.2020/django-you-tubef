from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django.http import Http404
from .models import Videos
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
@csrf_exempt
def notfound ():
    template = loader.get_template('djangoyoutube/not_found.html')
    context = {}
    return Http404(template.render(context))
@csrf_exempt
def index (request):
    if request.method == "POST":
        todo = request.POST['todo']
        id = request.POST['id']
        video = Videos.objects.get(id_video=id)
        if todo == "Seleccionar":
            video.selected = True
        elif todo == "Eliminar":
            video.selected = False
        video.save()
    lista = Videos.objects.all()
    template = loader.get_template('djangoyoutube/index.html')
    context = {
        'content_list': lista
    }
    return HttpResponse(template.render(context, request))

